﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Models;
using AliTec.Database;

namespace AliTec.Controllers {
    class EmployeeController : Controller {
        public EmployeeController() {

        }

        public Tuple<List<Employee>, List<Employee>> GetEmployees(bool withHistory) {
            Collection result = new Collection();

            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.EMPLOYEE)
                                        .SelectAll()
                                        .WithHistory(withHistory)
                                        .Build();



            result = connection.Select(builder);

            return Tuple.Create(result.resultList.Cast<Employee>().ToList(), result.historyList.Cast<Employee>().ToList());
        }

        public Tuple<List<Employee>, List<Employee>> GetEmployee(int id, bool withHistory) {
            Collection result = new Collection();

            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.EMPLOYEE)
                                        .Add("id", id)
                                        .WithHistory(withHistory)
                                        .Build();

            result = connection.Select(builder);

            return Tuple.Create(result.resultList.Cast<Employee>().ToList(), result.historyList.Cast<Employee>().ToList());
        }

        public bool CreateEmployee(string[] data) {
            string active = data[0];
            string title = data[1];
            string firstName = data[2];
            string lastName = data[3];
            string address = data[4];
            string houseNumber = data[5];
            int postCode = Convert.ToInt32(data[6]);
            string domicile = data[7];
            string email = data[8];
            string telNumber = data[9];
            string companyName = data[10];
            double salary = Convert.ToDouble(data[11]);
            string position = data[12];

            Employee employee = new Employee(active, title, firstName, lastName, address, houseNumber, postCode, domicile, email, telNumber, companyName, salary, position);

            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.EMPLOYEE)
                                        .AddModel(employee)
                                        .Build();

            return connection.Insert(builder);
        }

        public bool UpdateEmployee(int id, string[] data) {
            Dictionary<string, object> updateList = new Dictionary<string, object>();

            // The '*' (Asteriks) is used to check whether this field should be updated or not
            if (data[0] != "*")
            {
                updateList["active"] = data[0];
            }
            if(data[1] != "*")
            {
                updateList["title"] = data[1];
            }
            if (data[2] != "*")
            {
                updateList["firstName"] = data[2];
            }
            if (data[3] != "*")
            {
                updateList["lastName"] = data[3];
            }
            if (data[4] != "*")
            {
                updateList["address"] = data[4];
            }
            if (data[5] != "*")
            {
                updateList["houseNumber"] = data[5];
            }
            if (data[6] != "*")
            {
                updateList["postCode"] = Convert.ToInt32(data[6]);
            }
            if (data[7] != "*")
            {
                updateList["domicile"] = data[7];
            }
            if (data[8] != "*")
            {
                updateList["email"] = data[8];
            }
            if (data[9] != "*")
            {
                updateList["telNumber"] = data[9];
            }
            if (data[10] != "*")
            {
                updateList["companyName"] = data[10];
            }
            if (data[11] != "*")
            {
                updateList["salary"] = Convert.ToInt32(data[11]);
            }
            if (data[12] != "*")
            {
                updateList["position"] = data[12];
            }

            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.EMPLOYEE)
                                        .Add("id", id)
                                        .AddUpdateFieldRange(updateList)
                                        .Build();

            return connection.Update(builder);
        }

        public bool DeleteEmployee(int id) {
            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.EMPLOYEE)
                                        .Add("id", id)
                                        .Build();

            return connection.Delete(builder);
        }
    }
}

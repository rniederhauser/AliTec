﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Models;
using AliTec.Database;

namespace AliTec.Controllers {
    class OrderController {

        public Order GetOrder(int orderId) {
            var connection = new DatabaseConnection();

            var collection = connection.Select(new QueryBuilder(DatabaseCollections.ORDER)
                                                            .Add("id", orderId)
                                                            .Build());
            var orders = collection.resultList.Cast<Order>().ToList();

            return orders.Count > 0 ? orders[0] : null;
        }

        public Collection GetOrders() {
            var connection = new DatabaseConnection();

            var collection = connection.Select(new QueryBuilder(DatabaseCollections.ORDER)
                                                            .SelectAll()
                                                            .Build());

            return collection;
        }

        public bool DeleteOrder(int orderId) {
            var connection = new DatabaseConnection();

            return connection.Delete(new QueryBuilder(DatabaseCollections.ORDER).Add("id", orderId).Build());
        }

        public bool CancelOrder(int orderId) {
            var connection = new DatabaseConnection();

            var update = new QueryBuilder(DatabaseCollections.ORDER)
                                        .Add("id", orderId)
                                        .AddUpdateField("isCancelled", true)
                                        .Build();
            var select = new QueryBuilder(DatabaseCollections.ORDER)
                                        .Add("id", orderId)
                                        .Build();

            var orderCollection = connection.Select(select);

            if(orderCollection.resultList.Count > 0) {
                var orderList = orderCollection.resultList.Cast<Order>().ToList();

                if(orderList.Count > 0) {
                    int id = orderList[0].products[0].id;
                    int stock = orderList[0].products[0].stock;

                    var products = connection.Select(new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                                        .Add("id", id)
                                                        .Build()).resultList.Cast<Product>().ToList();

                    if(products.Count > 0) {
                        connection.Update(new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                                        .Add("id", id)
                                                        .AddUpdateField("stock", products[0].stock + stock)
                                                        .Build());

                        connection.Update(update);
                    }
                }


                return true;

            } else {
                return false;
            }
        }

        public Customer GetCustomer(int customerId) {
            var connection = new DatabaseConnection();
            var list = connection.Select(new QueryBuilder(DatabaseCollections.CUSTOMER).Add("id", customerId).Build()).resultList.Cast<Customer>().ToList();

            return list.Count > 0 ? list[0] : null;
        }

        public Product GetProduct(int id) {
            Collection result = new Collection();

            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                        .Add("id", id)
                                        .Build();

            result = connection.Select(builder);

            return result.resultList.Count > 0 ? result.resultList.Cast<Product>().ToList()[0] : null;
        }

        public bool CreateOrder(Order order) {
            if(order.products.Count > 0) {
                var connection = new DatabaseConnection();
                var builder = new QueryBuilder(DatabaseCollections.ORDER)
                                            .AddModel(order)
                                            .Build();

                var selectedProduct = order.products[0];
                var collection = connection.Select(new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                                        .Add("id", selectedProduct.id)
                                                        .Build());

                var product = collection.resultList.Cast<Product>().ToList();

                if(product.Count > 0) {

                    connection.Update(new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                            .Add("id", selectedProduct.id)
                                            .AddUpdateField("stock", selectedProduct.stock - product[0].stock)
                                            .Build());

                    return connection.Insert(builder);

                } else {
                    return false;
                }

            } else {
                return false;
            }
        }
    }
}

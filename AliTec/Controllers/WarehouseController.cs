﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Views;
using AliTec.Models;
using AliTec.Database;

namespace AliTec.Controllers {
    class WarehouseController : Controller {

        public WarehouseController() {
            
        }

        public Tuple<List<Product>, List<Product>> GetProducts(bool withHistory) {
            Collection result = new Collection();
            
            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                        .SelectAll()
                                        .WithHistory(withHistory)
                                        .Build();



            result = connection.Select(builder);

            return Tuple.Create(result.resultList.Cast<Product>().ToList(), result.historyList.Cast<Product>().ToList());
        }

        public Tuple<List<Product>, List<Product>> GetProduct(int id, bool withHistory) {
            Collection result = new Collection();

            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                        .Add("id", id)
                                        .WithHistory(withHistory)
                                        .Build();

            result = connection.Select(builder);

            return Tuple.Create(result.resultList.Cast<Product>().ToList(), result.historyList.Cast<Product>().ToList());
        }

        public bool CreateProduct(string[] data) {
            string storageSite = data[0];
            string name = data[1];
            string category = data[2];
            string manufacturer = data[3];
            int stock = Convert.ToInt32(data[4]);
            double price = Convert.ToDouble(data[5]);
            int minstock = Convert.ToInt32(data[6]);

            Product product = new Product(storageSite, name, category, manufacturer, stock, price, minstock);

            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                        .AddModel(product)
                                        .Build();

            return connection.Insert(builder);                                        
        }

        public bool UpdateProduct(int id, string[] data) {
            Dictionary<string, object> updateList = new Dictionary<string, object>();
            
            // The '*' (Asteriks) is used to check whether this field should be updated or not
            if(data[0] != "*") {
                updateList["storagesite"] = data[0];
            }
            if(data[1] != "*") {
                updateList["name"] = data[1];
            }
            if(data[2] != "*") {
                updateList["category"] = data[2];
            }
            if(data[3] != "*") {
                updateList["manufacturer"] = data[3];
            }
            if(data[4] != "*") {
                updateList["stock"] = Convert.ToInt32(data[4]);
            }
            if(data[5] != "*") {
                updateList["price"] = Convert.ToDouble(data[5]);
            }
            if(data[6] != "*") {
                updateList["minstock"] = Convert.ToInt32(data[6]);
            }

            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                        .Add("id", id)
                                        .AddUpdateFieldRange(updateList)
                                        .Build();

            return connection.Update(builder);
        }

        public bool DeleteProduct(int id) {
            var connection = new DatabaseConnection();
            var builder = new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                        .Add("id", id)
                                        .Build();

            return connection.Delete(builder);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Models;

namespace AliTec.Database {
    class Collection {

        private List<Model> list;
        private List<Model> historyList;

        public Collection() {
                
        }

        public Collection(List<Model> list) {
            this.list = list;
        }

        public Collection(List<Model> list, List<Model> historyList) : this(list) {
            this.historyList = historyList;
        }

        public void SetResult(List<Model> list) {
            this.list = list;
        }

        public void SetHistoryResult(List<Model> historyList) {
            this.historyList = historyList;
        }

        public List<Model> GetResult() {
            return this.list;
        }

        public List<Model> GetHistoryResult() {
            return this.historyList;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Models;

namespace AliTec.Database {

    class DatabaseCollections {

        // Files
        public static string WAREHOUSE = "warehouse.bin";
        public static string EMPLOYEE = "employee.bin";
        public static string CUSTOMER = "customer.bin";
        public static string ORDER = "order.bin";

        public static List<string> GetCollections() {
            List<string> list = new List<string>();

            list.Add(WAREHOUSE);
            list.Add(EMPLOYEE);
            list.Add(CUSTOMER);
            list.Add(ORDER);

            return list;
        }

    }

    class Collection {

        public List<Model> resultList { get; set; }
        public List<Model> historyList { get; set; }

        public Collection() {
            this.resultList = new List<Model>();
            this.historyList = new List<Model>();
        }

        public Collection(List<Model> resultList, List<Model> historyList) {
            this.resultList = resultList;
            this.historyList = historyList;
        }

    }
}

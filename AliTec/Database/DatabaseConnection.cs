﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using AliTec.Models;

namespace AliTec.Database {
    class DatabaseConnection {

        public static string PATH = @"C:/Temp/AliTec/";

        public DatabaseConnection() {
        }

        public static string GetFinalPath(string path) {
            return PATH + path;
        }

        public Collection Select(QueryBuilder builder) {
            List<Model> result = new List<Model>();
            List<Model> resultHistory = new List<Model>();
            Collection collection = new Collection();

            try {

                using (FileStream fs = new FileStream(GetFinalPath(builder.GetFileName()), FileMode.Open, FileAccess.Read)) {
                    // check if file isn't empty
                    if (fs.Length > 0) {
                        BinaryFormatter formatter = new BinaryFormatter();
                        result = (List<Model>) formatter.Deserialize(fs);

                    }
                }

                // TODO: check this select
                if (builder.IsWithHistory()) {
                    using (FileStream fs = new FileStream(GetFinalPath(builder.GetBackupFileName()), FileMode.Open, FileAccess.Read)) {
                        // check if file isn't empty
                        if (fs.Length > 0) {
                            BinaryFormatter formatter = new BinaryFormatter();
                            resultHistory = (List<Model>) formatter.Deserialize(fs);
                        }
                    }
                }

            } catch (IOException e) {

            }

            // check if there are criterias
            if (builder.IsSelectAll()) {
                collection = new Collection(result, resultHistory);
            } else {
                var resultList = new List<Model>();
                var historyList = new List<Model>();

                // loop through initial result
                for (int i = 0; i < result.Count; i++) {
                    var model = result[i];
                    var properties = model.GetType().GetProperties();

                    // go through each property of the model
                    foreach (var property in properties) {
                        // go through each criteria of the builder map
                        foreach (var fieldItem in builder.GetMap()) {
                            // check if criteria is available in the properties
                            if (property.Name == fieldItem.Key) {
                                // check if value is equal
                                // TODO: add string operation support

                                if (property.GetValue(model).Equals(fieldItem.Value)) {
                                    resultList.Add(model);
                                }
                            }
                        }
                    }
                }

                // loop through history result
                for (int i = 0; i < resultHistory.Count; i++) {
                    var model = result[i];
                    var properties = model.GetType().GetProperties();

                    // go through each property of the model
                    foreach (var property in properties) {
                        // go through each criteria of the builder map
                        foreach (var fieldItem in builder.GetMap()) {
                            // check if criteria is available in the properties
                            if (property.Name == fieldItem.Key) {
                                // check if value is equal
                                // TODO: add string operation support
                                if (property.GetValue(model).Equals(fieldItem.Value)) {
                                    historyList.Add(model);
                                }
                            }
                        }
                    }
                }

                collection = new Collection(resultList, historyList);
            }

            return collection;
        }

        public bool Insert(QueryBuilder builder) {

            try {

                using (FileStream fs = new FileStream(GetFinalPath(builder.GetFileName()), FileMode.Append, FileAccess.Write)) {

                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fs, builder.GetModels());

                }

                return true;

            } catch (IOException e) {
                return false;
            }

        }
        
        public bool Update(QueryBuilder builder) {

            try {

                Collection dbCollection = Select(new QueryBuilder(builder.GetRawFileName()).SelectAll());
                Collection collection = new Collection();
                
                foreach (var model in dbCollection.resultList) {
                    var type = model.GetType();

                    // go through each model until one with the given criteria occurs
                    if (type.GetProperty("id").GetValue(model).Equals(builder.GetMap()["id"])) {
                        collection.historyList.Add(model); // save unchanged model to history

                        // update each field
                        foreach (var fieldValueItem in builder.GetUpdateMap()) {
                            type.GetProperty(fieldValueItem.Key).SetValue(model, fieldValueItem.Value);
                        }

                        collection.resultList.Add(model);
                    } else {
                        collection.resultList.Add(model);  
                    }
                }

                // update in main file
                using (FileStream fs = new FileStream(GetFinalPath(builder.GetFileName()), FileMode.Open, FileAccess.Write)) {

                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fs, collection.resultList);

                }

                // insert copy into history file
                using (FileStream fs = new FileStream(GetFinalPath(builder.GetBackupFileName()), FileMode.Append, FileAccess.Write)) {

                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fs, collection.historyList);

                }

                return true;

            } catch (IOException e) {
                return false;
            }

        }

        public bool Delete(QueryBuilder builder) {

            try {

                Collection resultCollection = Select(new QueryBuilder(builder.GetRawFileName()).SelectAll().Build());
                Collection collection = new Collection();

                foreach (var model in resultCollection.resultList) {
                    var type = model.GetType();

                    if (type.GetProperty("id").GetValue(model).Equals(builder.GetMap()["id"])) {
                        collection.historyList.Add(model);
                    } else {
                        collection.resultList.Add(model);
                    }
                }


                // update file
                using (FileStream fs = new FileStream(GetFinalPath(builder.GetFileName()), FileMode.Open, FileAccess.Write)) {

                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fs, collection.resultList);

                }


                // add to history
                using (FileStream fs = new FileStream(GetFinalPath(builder.GetBackupFileName()), FileMode.Append, FileAccess.Write)) {

                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fs, collection.historyList);

                }

                return true;

            } catch (IOException e) {
                return false;
            }

        }
    }
}

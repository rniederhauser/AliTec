﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliTec.Database {
    class DatabaseManager {

        public DatabaseManager() {
            var migration = new DatabaseMigration();
            migration.Migrate();
        }
    }
}

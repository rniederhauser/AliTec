﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AliTec.Database;

namespace AliTec.Database {
    class DatabaseMigration {

        public void Migrate() {
            if (!Directory.Exists(DatabaseConnection.PATH)) {
                Directory.CreateDirectory(DatabaseConnection.PATH);
            }

            foreach (var file in DatabaseCollections.GetCollections()) {
                // create (backup) file if not exists
                using (var fileStream = File.Open(DatabaseConnection.GetFinalPath(file), FileMode.OpenOrCreate, FileAccess.Read)) { }
                using (var fileStream = File.Open(DatabaseConnection.GetFinalPath("backup_" + file), FileMode.OpenOrCreate, FileAccess.Read)) { }
            }
        }

    }
}

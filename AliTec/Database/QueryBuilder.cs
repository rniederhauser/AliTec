﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Models;

namespace AliTec.Database {

    class QueryBuilder {

        // This Dictionary contains the criterias for the select from the db
        private Dictionary<string, object> map;
        // The updateList Dictionary contains the attributes and corresponding values that will be updated
        private Dictionary<string, object> updateList;
        // Filename that is accessed during query
        private string filename;
        // Boolean to check whether user explicitly wants to get the result of the history file
        private bool withHistory;
        // Boolean to check whether user wants to get all objects from file
        private bool selectAll;
        // List that contains the Models that you want to insert into the Database
        private List<Model> modelList;

        public QueryBuilder(string filename) {
            this.filename = filename;

            // initialize
            map = new Dictionary<string, object>();
            updateList = new Dictionary<string, object>();
            modelList = new List<Model>();
        }

        // Returns the filename with extension of the real file
        public string GetFileName() {
            return filename.EndsWith(".bin") ? filename : filename + ".bin";
        }

        public string GetRawFileName() {
            return filename;
        }


        // Returns the filename with extension of the history file
        public string GetBackupFileName() {
            return filename.EndsWith(".bin") ? "backup_" + filename : "backup_" + filename + ".bin";
        }

        // The DBConnection Class uses this method to access the attribute
        public bool IsSelectAll() {
            return selectAll;
        }

        // Is used in the Insert Function to get the models
        public List<Model> GetModels() {
            return modelList;
        }

        // Gets the Criteria Dictionary for the Select
        public Dictionary<string, object> GetMap() {
            return map;
        }

        // Gets the Update Dictionary for the Update Function
        public Dictionary<string, object> GetUpdateMap() {
            return updateList;
        }

        // Is used in the Select Function
        public bool IsWithHistory() {
            return withHistory;
        }

        // Adds a new criteria for the Select Function
        public QueryBuilder Add(string field, object value) {
            map.Add(field, value);
            return this;
        }

        // Adds a new model for the Insert Function
        public QueryBuilder AddModel(Model model) {
            modelList.Add(model);
            return this;
        }

        // Adds a update field/value pair that will be updated in the db
        public QueryBuilder AddUpdateField(string property, object value) {
            updateList.Add(property, value);
            return this;
        }

        // Adds a update field/value pair that will be updated in the db
        public QueryBuilder AddUpdateFieldRange(Dictionary<string, object> list) {
            foreach(KeyValuePair<string, object> item in list) {
                updateList.Add(item.Key, item.Value);
            }

            return this;
        }

        // Add a list of models that will be updated
        public QueryBuilder AddModelRange(List<Model> list) {
            modelList.AddRange(list);
            return this;
        }

        // Use this method to get the historyList as a result
        public QueryBuilder WithHistory() {
            withHistory = true;
            return this;
        }

        // Use this method to get the historyList as a result
        public QueryBuilder WithHistory(bool value) {
            withHistory = value;
            return this;
        }

        // Use this method to select all results from db
        public QueryBuilder SelectAll() {
            selectAll = true;
            return this;
        }

        // Get the QueryBuilder Class
        public QueryBuilder Build() {
            return this;
        }
    }
}

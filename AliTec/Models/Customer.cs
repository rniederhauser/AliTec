﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliTec.Models {
    [Serializable]
    class Customer : Model {
        
        public int id { get; set; }
        public string active { get; set; }
        public string companyName { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string address { get; set; }
        public string houseNumber { get; set; }
        public int postCode { get; set; }
        public string domicile { get; set; }
        public string email { get; set; }
        public string telNumber { get; set; }
        public string comment { get; set; }
        
        public Customer() {
            id = GenerateId();
        }

        public Customer(int id, string active, string firstName, string lastName, string address, string houseNumber, int postCode, string domicile, string email, string telNumber, string companyName, string comment, string title) {
            this.id = id;
            this.active = active;
            this.companyName = companyName;
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.houseNumber = houseNumber;
            this.postCode = postCode;
            this.domicile = domicile;
            this.email = email;
            this.telNumber = telNumber;
            this.comment = comment;
            this.title = title;
        }
        


        public Customer(string active, string firstName, string lastName, string address, string houseNumber, int postCode, string domicile, string email, string telNumber, string companyName,string comment, string title) {
            id = GenerateId();
            this.active = active;
            this.companyName = companyName;
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.houseNumber = houseNumber;
            this.postCode = postCode;
            this.domicile = domicile;
            this.email = email;
            this.telNumber = telNumber;
            this.comment = comment;
            this.title = title;
        }
    }
}
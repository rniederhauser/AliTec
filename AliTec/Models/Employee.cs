﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliTec.Models
{
    [Serializable]
    class Employee : Model {

        public int id { get; set; }
        public string active { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string address { get; set; }
        public string houseNumber { get; set; }
        public int postCode { get; set; }
        public string domicile { get; set; }
        public string email { get; set; }
        public string telNumber { get; set; }
        public string companyName { get; set; }
        public double salary { get; set; }
        public string position { get; set; }

        public Employee(){
            id = GenerateId();
        }    

        public Employee(int id, string active, string title, string firstName, string lastName, string address, string houseNumber, int postCode, string domicile, string email, string telNumber, string companyName, double salary, string position) {
            this.id = id;
            this.active = active;
            this.companyName = companyName;
            this.title = title;
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.houseNumber = houseNumber;
            this.postCode = postCode;
            this.domicile = domicile;
            this.email = email;
            this.telNumber = telNumber;
            this.position = position;
            this.salary = salary;
        }

        public Employee(string active, string title, string firstName, string lastName, string address, string houseNumber, int postCode, string domicile, string email, string telNumber, string companyName, double salary, string position) {
            id = GenerateId();
            this.active = active;
            this.title = title;
            this.companyName = companyName;
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.houseNumber = houseNumber;
            this.postCode = postCode;
            this.domicile = domicile;
            this.email = email;
            this.telNumber = telNumber;
            this.position = position;
            this.salary = salary;
        }

    }
}

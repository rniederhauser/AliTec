﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliTec.Models {
    [Serializable]
    class Model {

        public int id { get; set; }

        public int GenerateId() {
            return new Random().Next(100000);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliTec.Models {

    [Serializable]
    class Order : Model {

        public int id { get; set; }
        public int customerId { get; set; }
        public DateTime orderDate { get; set; }
        public List<Product> products { get; set; }
        public bool isCancelled { get; set; }

        public Order(int customerId, DateTime orderDate, bool isCancelled) {
            this.id = GenerateId();
            this.customerId = customerId;
            this.orderDate = orderDate;
            this.products = new List<Product>();
            this.isCancelled = isCancelled;
        }

        public Order(int id, int customerId, DateTime orderDate, bool isCancelled) : this(customerId, orderDate, isCancelled) {
            this.id = id;
        }

        public void AddProduct(Product product, int amount) {
            product.stock = amount;
            products.Add(product);
        }

        public bool RemoveProduct(int id) {
            var newList = new List<Product>();
            var removed = false;

            foreach (var product in products) {
                if(product.id != id) {
                    newList.Add(product);
                } else {
                    removed = true;
                }
            }

            products = newList;

            return removed;
        }

        public bool EditProduct(string[] data) {
            try {

                int productId = Convert.ToInt32(data[0]);
                int amount = Convert.ToInt32(data[2]);

                foreach(var product in products) {
                    if(product.id == productId) {
                        product.stock = amount;
                    }
                }

                return true;

            } catch(Exception) {
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliTec.Models {
    class Person {

        public int title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }

        public string street { get; set; }
        public string streetNumber { get; set; }
        public string country { get; set; }
        public int postalCode { get; set; }
        public string city { get; set; }

        public int gender { get; set; }

        public Person(string firstname, string lastname) {
            this.firstname = firstname;
            this.lastname = lastname;
        }

        public Person(string firstname, string lastname, int gender) : this(firstname, lastname) {
            this.gender = gender;
        }

        public Person(string firstname, string lastname, string street, string streetNumber, string country,
            int postalCode, string city) : this(firstname, lastname) {
            this.street = street;
            this.streetNumber = streetNumber;
            this.country = country;
            this.postalCode = postalCode;
            this.city = city;
        }

        public static string GetTitle(int title) {
            return title == 0 ? "Herr" : "Frau";
        }

        public static string GetGender(int gender) {
            return gender == 0 ? "Maskulin" : "Feminin";
        }

    }
}

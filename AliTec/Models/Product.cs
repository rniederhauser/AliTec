﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Database;

namespace AliTec.Models {
    [Serializable]
    class Product : Model {

        public static int NOT_FOUND = -1;

        public int id { get; set; }
        public string storagesite { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public string manufacturer { get; set; }
        public int stock { get; set; }
        public double price { get; set; }
        public int minstock { get; set; }

        public Product() {
            id = GenerateId();
        }

        public Product(int id) {
            this.id = id;

            var conn = new DatabaseConnection();
            var collection = conn.Select(new QueryBuilder(DatabaseCollections.WAREHOUSE).Add("id", id).Build());

            var resultList = collection.resultList.Cast<Product>().ToList();
            
            if(resultList.Count == 1) {
                Product product = resultList[0];
                storagesite = product.storagesite;
                name = product.name;
                category = product.category;
                manufacturer = product.manufacturer;
                stock = product.stock;
                price = product.price;
                minstock = product.minstock;
            } else {
                id = NOT_FOUND;
            }
        }

        public Product(int id, string storagesite, string name, string category, string manufacturer, int stock, double price, int minstock) {
            this.id = id;
            this.storagesite = storagesite;
            this.name = name;
            this.category = category;
            this.manufacturer = manufacturer;
            this.stock = stock;
            this.price = price;
            this.minstock = minstock;
        }

        public Product(string storagesite, string name, string category, string manufacturer, int stock, double price, int minstock) {
            id = GenerateId();
            this.storagesite = storagesite;
            this.name = name;
            this.category = category;
            this.manufacturer = manufacturer;
            this.stock = stock;
            this.price = price;
            this.minstock = minstock;
        }
    }
}

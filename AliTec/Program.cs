﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AliTec.Views;
using AliTec.Database;
using AliTec.Models;

namespace AliTec {
    class Program {
        static void Main(string[] args) {
            var dbMigration = new DatabaseMigration();
            dbMigration.Migrate();

            string action = "";

            while(action != "0") {

                PrintMenu();

                Console.Write("Please enter action: ");
                action = Console.ReadLine();

                switch(action) {
                    case "1": {
                        var view = new WarehouseView();
                        view.Run();
                        break;
                    }

                    case "2": {
                        var view = new CustomerView();
                        view.Run();
                        break;
                    }

                    case "3": {
                        var view = new EmployeeView();
                        view.Run();
                        break;
                    }

                    case "4": {
                        var view = new OrderView();
                        view.Run();
                        break;
                    }
                }
            }
        }

        public static void PrintMenu() {
            Console.WriteLine("----------------------------");
            Console.WriteLine("0 Exit");
            Console.WriteLine("1 Show Warehouse Management");
            Console.WriteLine("2 Show Customer Management");
            Console.WriteLine("3 Show Employee Management");
            Console.WriteLine("4 Show Order Management");
            Console.WriteLine("----------------------------");
        }

    }

}
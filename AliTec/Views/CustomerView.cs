﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Controllers;
using AliTec.Database;
using AliTec.Models;

namespace AliTec.Views {
    class CustomerView {
        private CustomerController controller;

        public CustomerView() {
            controller = new CustomerController();
        }

        public void PrintMenu() { 
            Console.WriteLine();
            Console.WriteLine("Menu Customer Management");
            Console.WriteLine("----------------------------");
            Console.WriteLine("1 Create an Customer");
            Console.WriteLine("2 Show an Customer by id");
            Console.WriteLine("3 Show all Customers");
            Console.WriteLine("4 Edit an Customer");
            Console.WriteLine("5 Delete an Customer");
            Console.WriteLine("0 Back");
        }

        public int GetCustomerID() {
            bool success = false;
            int id = 0;
            while (!success) {
                try {
                    Console.Write("Customer ID: ");
                    id = Convert.ToInt32(Console.ReadLine());
                    success = true;
                } catch (Exception e) {

                }
            }
            return id;
        }

        public void Run() {

            try {
                int action = -1;

                do {
                    PrintMenu();

                    string input = Console.ReadLine();
                    action = Convert.ToInt32(input);

                    Handle(action);

                } while (action != 0);

            } catch (Exception e){

            }
        }

        public void Handle(int action) {
            switch (action){
                case 1: {
                        Console.WriteLine("Enter: Active;Title;Firstname;Lastname;Address;Housenumber;Postcode;Domicile;EMail;Telephonenumber;CompanyName;Comment");
                        string[] data = Console.ReadLine().Split(';');

                        bool result = controller.CreateCustomer(data);

                        if (result)
                        {
                            Console.WriteLine("Customer successfully added!");
                        }
                        else
                        {
                            Console.WriteLine("Whoops! Error occurred.. Try again!");
                        }

                        break;
                    }

                case 2:
                    {
                        int id = GetCustomerID();

                        Console.Write("With History? (y/n): ");
                        string response = Console.ReadLine();
                        bool withHistory = false;

                        if (response == "y")
                        {
                            withHistory = true;
                        }

                        var customers = controller.GetCustomer(id, withHistory);

                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("Real:");
                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("Customer ID, Active, Title, Firstname, Lastname, Address, Housenumber, Postcode, Domicile, EMail, Telephone number, Company Name, Comment");
                        foreach (var customer in customers.Item1)
                        {
                            Console.WriteLine(customer.id + ", " + customer.active + ", " + customer.title + ", "  + customer.firstName + ", " + customer.lastName + ", " + customer.address + ", " + customer.houseNumber+ ", " + customer.postCode+ ", " + customer.email + ", " + customer.telNumber + ", " + customer.companyName + ", " + customer.comment);
                        }

                        if (withHistory)
                        {
                            Console.WriteLine("--------------------------------");
                            Console.WriteLine("History:");
                            Console.WriteLine("--------------------------------");
                            Console.WriteLine("Customer ID, Active, Title, Firstname, Lastname, Address, Housenumber, Postcode, Domicile, EMail, Telephone number, Company Name, Comment");
                            foreach (var customer in customers.Item2)
                            {
                                Console.WriteLine(customer.id + ", " + customer.active + ", " + customer.title + ", " + customer.firstName + ", " + customer.lastName + ", " + customer.address + ", " + customer.houseNumber + ", " + customer.postCode + ", " + customer.email + ", " + customer.telNumber + ", " + customer.companyName + ", " + customer.comment);
                            }
                        }
                        break;
                    }

                case 3:
                    {
                        Console.Write("With History? (y/n): ");
                        string response = Console.ReadLine();
                        bool withHistory = false;

                        if (response == "y")
                        {
                            withHistory = true;
                        }

                        var customers = controller.GetCustomers(withHistory);

                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("Real:");
                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("Customer ID, Active, Title, Firstname, Lastname, Address, Housenumber, Postcode, Domicile, EMail, Telephone number, Company name, Salary, Position");
                        foreach (var customer in customers.Item1)
                        {

                            Console.WriteLine(customer.id + ", " + customer.active + ", " + customer.title + ", " + customer.firstName + ", " + customer.lastName + ", " + customer.address + ", " + customer.houseNumber + ", " + customer.postCode + ", " + customer.email + ", " + customer.telNumber + ", " + customer.companyName + ", " + customer.comment);
                        }

                        if (withHistory)
                        {
                            Console.WriteLine("--------------------------------");
                            Console.WriteLine("History:");
                            Console.WriteLine("--------------------------------");
                            Console.WriteLine("Customer ID, Active, Title, Firstname, Lastname, Address, Housenumber, Postcode, Domicile, EMail, Telephone number, Company Name, Comment");

                            foreach (var customer in customers.Item2)
                            {

                                Console.WriteLine(customer.id + ", " + customer.active + ", " + customer.title + ", " + customer.firstName + ", " + customer.lastName + ", " + customer.address + ", " + customer.houseNumber + ", " + customer.postCode + ", " + customer.email + ", " + customer.telNumber + ", " + customer.companyName + ", " + customer.comment);
                            }
                        }
                        break;
                    }

                case 4:
                    {
                        int id = GetCustomerID();

                        Console.WriteLine("Enter a '*' to keep the current value of the field!");
                        Console.WriteLine("Enter: Active;Title;Firstname;Lastname;Address;Housenumber;Postcode;Domicile;EMail;Telephonenumber;CompanyName;Comment");
                        string[] data = Console.ReadLine().Split(';');

                        bool success = controller.UpdateCustomer(id, data);

                        if (success)
                        {
                            Console.WriteLine("Customer successfully updated!");
                        }
                        else
                        {
                            Console.WriteLine("Whoops! Error occurred.. Try again!");
                        }

                        break;
                    }

                case 5:
                    {
                        int id = GetCustomerID();
                        bool success = controller.DeleteCustomer(id);

                        if (success)
                        {
                            Console.WriteLine("Customer successfully deleted!");
                        }
                        else
                        {
                            Console.WriteLine("Whoops! Error occurred.. Try again!");
                        }

                        break;
                    }
            }
        }
    }
}

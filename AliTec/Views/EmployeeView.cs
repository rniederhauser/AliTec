﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Controllers;
using AliTec.Database;
using AliTec.Models;

namespace AliTec.Views
{
    class EmployeeView : IView
    {
        private EmployeeController controller;

        public EmployeeView()
        {
            controller = new EmployeeController();
        }

        public void PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Menu Employee Management");
            Console.WriteLine("----------------------------");
            Console.WriteLine("1 Create an Employee");
            Console.WriteLine("2 Show an Employee by id");
            Console.WriteLine("3 Show all Employees");
            Console.WriteLine("4 Edit an Employee");
            Console.WriteLine("5 Delete an Employee");
            Console.WriteLine("0 Back");
        }

        public int GetEmployeeID()
        {
            bool success = false;
            int id = 0;
            while (!success)
            {
                try
                {
                    Console.Write("Employee ID: ");
                    id = Convert.ToInt32(Console.ReadLine());
                    success = true;
                }
                catch (Exception e)
                {

                }
            }
            return id;
        }

        public void Run()
        {

            try
            {
                int action = -1;

                do
                {
                    PrintMenu();

                    string input = Console.ReadLine();
                    action = Convert.ToInt32(input);

                    Handle(action);

                } while (action != 0);

            }
            catch (Exception e)
            {

            }
        }

        public void Handle(int action)
        {
            switch (action)
            {
                case 1:
                    {
                        Console.WriteLine("Enter: Active;Title;Firstname;Lastname;Address;Housenumber;Postcode;Domicile;EMail;Telephonenumber;Companyname;Salary;Position");
                        string[] data = Console.ReadLine().Split(';');

                        bool result = controller.CreateEmployee(data);

                        if (result)
                        {
                            Console.WriteLine("Employee successfully added!");
                        }
                        else
                        {
                            Console.WriteLine("Whoops! Error occurred.. Try again!");
                        }

                        break;
                    }

                case 2:
                    {
                        int id = GetEmployeeID();

                        Console.Write("With History? (y/n): ");
                        string response = Console.ReadLine();
                        bool withHistory = false;

                        if (response == "y")
                        {
                            withHistory = true;
                        }

                        var employees = controller.GetEmployee(id, withHistory);

                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("Real:");
                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("Employee ID, Active, Title, Firstname, Lastname, Address, Housenumber, Postcode, Domicile, EMail, Telephone number, Company name, Salary, Position");
                        foreach (var employee in employees.Item1)
                        {
                            Console.WriteLine(employee.id + ", " + employee.active + ", " + employee.title + ", " + employee.firstName + ", " + employee.lastName + ", " + employee.address + ", " + employee.houseNumber + ", " + employee.postCode + ", " + employee.email + ", " + employee.telNumber + ", " + employee.companyName + ", " + employee.salary + ", " + employee.position);
                        }

                        if (withHistory)
                        {
                            Console.WriteLine("--------------------------------");
                            Console.WriteLine("History:");
                            Console.WriteLine("--------------------------------");
                            Console.WriteLine("Employee ID, Active, Title, Firstname, Lastname, Address, Housenumber, Postcode, Domicile, EMail, Telephone number, Company name, Salary, Position");

                            foreach (var employee in employees.Item2)
                            {
                                Console.WriteLine(employee.id + ", " + employee.active + ", " + employee.title + ", " + employee.firstName + ", " + employee.lastName + ", " + employee.address + ", " + employee.houseNumber + ", " + employee.postCode + ", " + employee.email + ", " + employee.telNumber + ", " + employee.companyName + ", " + employee.salary + ", " + employee.position);
                            }
                        }
                        break;
                    }

                case 3:
                    {
                        Console.Write("With History? (y/n): ");
                        string response = Console.ReadLine();
                        bool withHistory = false;

                        if (response == "y")
                        {
                            withHistory = true;
                        }

                        var employees = controller.GetEmployees(withHistory);

                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("Real:");
                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("Employee ID, Active, Title, Firstname, Lastname, Address, Housenumber, Postcode, Domicile, EMail, Telephone number, Company name, Salary, Position");
                        foreach (var employee in employees.Item1)
                        {
                            Console.WriteLine(employee.id + ", " + employee.active + ", " + employee.title + ", " + employee.firstName + ", " + employee.lastName + ", " + employee.address + ", " + employee.houseNumber + ", " + employee.postCode + ", " + employee.email + ", " + employee.telNumber + ", " + employee.companyName + ", " + employee.salary + ", " + employee.position);
                        }
                        if (withHistory)
                        {
                            Console.WriteLine("--------------------------------");
                            Console.WriteLine("History:");
                            Console.WriteLine("--------------------------------");
                            Console.WriteLine("Employee ID, Active, Title, Firstname, Lastname, Address, Housenumber, Postcode, Domicile, EMail, Telephone number, Company name, Salary, Position");
                            foreach (var employee in employees.Item2)
                            {
                                Console.WriteLine(employee.id + ", " + employee.active + ", " + employee.title + ", " + employee.firstName + ", " + employee.lastName + ", " + employee.address + ", " + employee.houseNumber + ", " + employee.postCode + ", " + employee.email + ", " + employee.telNumber + ", " + employee.companyName + ", " + employee.salary + ", " + employee.position);
                            }
                        }
                        break;
                    }

                case 4:
                    {
                        int id = GetEmployeeID();

                        Console.WriteLine("Enter a '*' to keep the current value of the field!");
                        Console.WriteLine("Enter: Active;Title;Firstname;Lastname;Address;Housenumber;Postcode;Domicile;EMail;Telephonenumber;Companyname;Salary;Position");
                        string[] data = Console.ReadLine().Split(';');

                        bool success = controller.UpdateEmployee(id, data);

                        if (success)
                        {
                            Console.WriteLine("Employee successfully updated!");
                        }
                        else
                        {
                            Console.WriteLine("Whoops! Error occurred.. Try again!");
                        }

                        break;
                    }

                case 5:
                    {
                        int id = GetEmployeeID();
                        bool success = controller.DeleteEmployee(id);

                        if (success)
                        {
                            Console.WriteLine("Employee successfully deleted!");
                        }
                        else
                        {
                            Console.WriteLine("Whoops! Error occurred.. Try again!");
                        }

                        break;
                    }
            }
        }
    }
}

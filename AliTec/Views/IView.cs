﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliTec.Views { 
    interface IView {

        // Prints the menu
        // -----------------------------------
        // 0 is attributed to leave the view
        // The rest should be styled like this:
        // 1 First Action
        // 2 Second Action
        // .. etc.
        // 
        void PrintMenu();

        // Main Method of the View
        // ----------------------------------
        // Keeps the view running and thus, needs a while loop which will loop until a break condition
        //
        void Run();

        // Handle Method for user input
        // -----------------------------------
        // handles the action, entered by the user
        // uses a switch case structure
        // -----------------------------------
        // int action: the action that is listed on the menu
        //
        void Handle(int action);
    }
}

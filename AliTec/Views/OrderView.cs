﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Models;
using AliTec.Controllers;
using System.IO;
using AliTec.Database;

namespace AliTec.Views {
    class OrderView : IView {

        private OrderController controller;
        private Order order;

        public OrderView() {
            controller = new OrderController();
            order = null;
        }

        public void PrintMenu() {
            Console.WriteLine();
            Console.WriteLine("Menu - Place an Order");
            Console.WriteLine("----------------------------");
            Console.WriteLine("1 Add a product to the order");
            Console.WriteLine("2 Edit a product of the order");
            Console.WriteLine("3 Remove a product from the order");
            Console.WriteLine("4 Complete Order");
            Console.WriteLine("5 Show Order by ID");
            Console.WriteLine("6 Show all Orders");
            Console.WriteLine("7 Cancel Order");
            Console.WriteLine("8 Delete an Order by ID");
            Console.WriteLine("9 Show current order");
            Console.WriteLine("0 Back");
        }

        // ask a user for a valid number with a certain text
        public int GetUserInputNumber(string prompt) {
            bool success = false;
            int id = 0;
            while (!success) {
                try {
                    Console.Write(prompt);
                    id = Convert.ToInt32(Console.ReadLine());
                    success = true;
                } catch (Exception e) {

                }
            }
            return id;
        }

        public void Handle(int action) {
            switch (action) {
                case 1: {
                    Console.WriteLine("Enter: Product ID;Customer ID;Amount");
                    string[] data = Console.ReadLine().Split(';');

                    if(order == null) {
                        order = new Order(Convert.ToInt32(data[1]), DateTime.Now, false);
                    }

                    var product = new Product(Convert.ToInt32(data[0]));

                    if(product.id != Product.NOT_FOUND) {
                        order.AddProduct(product, Convert.ToInt32(data[2]));
                        Console.WriteLine("Product successfully added to Order!");
                    } else {
                        Console.WriteLine("Whoops! Product wasn't found..");
                    }

                    break;
                }

                case 2: {
                    Console.WriteLine("Enter a '*' to keep the current value of the field!");
                    Console.WriteLine("Enter: Product ID;Customer ID;Amount");
                    string input = Console.ReadLine();
                    string[] data = input.Split(';');

                    if(order.EditProduct(data)) {
                        Console.WriteLine("Product was successfully updated!");
                    } else {
                        Console.WriteLine("Error occurred! Try again..");
                    }

                    break;
                }

                case 3: {
                    int productId = GetUserInputNumber("Enter Product ID: ");

                    if(order.RemoveProduct(productId)) {
                        Console.WriteLine("Product has been removed successfully!");
                    } else {
                        Console.WriteLine("Whoops! Product wasn't found..");
                    }

                    break;
                }

                case 4: {
                    string downloadFolder = DatabaseConnection.GetFinalPath("bill.txt");

                    using(FileStream stream = new FileStream(downloadFolder, FileMode.Create)) {

                        StreamWriter writer = new StreamWriter(stream);

                        var customer = controller.GetCustomer(order.customerId);

                        if(customer != null) {
                            writer.WriteLine("                                                                       " + customer.title);
                            writer.WriteLine("                                                                       " + customer.firstName + " " + customer.lastName);
                            writer.WriteLine("                                                                       " + customer.address + " " + customer.houseNumber);
                            writer.WriteLine("                                                                       " + customer.postCode + " " + customer.domicile);
                            writer.WriteLine("Customer Id: " + customer.id);
                            writer.WriteLine("Order Date: " + order.orderDate.ToString());
                            writer.WriteLine("");
                            writer.WriteLine("");
                            writer.WriteLine("Position  Amount      Product              Description                 Price/Unit        Sum");
                            writer.WriteLine("--------------------------------------------------------------------------------------------");

                            int position = 1;
                            double subtotal = 0;
                            foreach (var element in order.products) {
                                var product = controller.GetProduct(element.id);

                                if(product != null) {
                                    subtotal += (product.price * product.stock);
                                    writer.WriteLine(position.ToString().PadRight(10, ' ')
                                              + product.stock.ToString().PadRight(12, ' ')
                                              + product.name.PadRight(21, ' ')
                                              + product.category.PadRight(28, ' ')
                                              + product.price.ToString("N2").PadRight(18, ' ')
                                              + (product.price * product.stock).ToString("N2"));

                                    position++;
                                }                               
                            }

                            double mwst = subtotal * 0.08;
                            double total = mwst + subtotal;

                            writer.WriteLine("Subtotal".PadLeft(87, ' ') + subtotal.ToString("N2").PadLeft(10, ' '));
                            writer.WriteLine("MWST 8%".PadLeft(87, ' ') + mwst.ToString("N2").PadLeft(10, ' '));
                            writer.WriteLine("Total".PadLeft(87, ' ') + total.ToString("N2").PadLeft(10, ' '));

                            writer.WriteLine("");
                            writer.WriteLine("");
                            writer.WriteLine("");
                            writer.WriteLine("");

                            writer.WriteLine("            Thanks for your order!");
                            writer.WriteLine(" We kindly ask you to pay the bill within 30 days");

                        }

                        

                        writer.Close();

                    }

                    controller.CreateOrder(order);

                    Console.WriteLine("Order is complete!");

                    break;
                }

                case 5: {
                    int orderId = GetUserInputNumber("Enter Order ID: ");

                    Order finalOrder;
                    

                    if(order != null && order.id == orderId) {
                        finalOrder = order;
                    } else {
                        finalOrder = controller.GetOrder(orderId);
                    }

                    
                    Console.WriteLine("Order ID, Customer ID");
                    Console.WriteLine(finalOrder.id + ", " + finalOrder.customerId);
                    Console.WriteLine("---------------------");
                    Console.WriteLine("Product ID, Amount");
                    
                    foreach(var product in finalOrder.products) {
                        Console.WriteLine(product.id + ", " + product.stock);
                    }


                    break;
                }

                case 6: {

                    var collection = controller.GetOrders();
                    var orders = collection.resultList.Cast<Order>().ToList();

                    Console.WriteLine("-----------------------------");
                    Console.WriteLine("Orders");
                    Console.WriteLine("-----------------------------");

                    Console.WriteLine("Order ID, Customer ID, Order Date");
                    foreach (var order in orders) {
                        if (!order.isCancelled) {
                            Console.WriteLine(order.id + ", " + order.customerId + ", " + order.orderDate);

                            Console.WriteLine("    Products:");
                            Console.WriteLine("    Product ID, Amount");

                            foreach (var product in order.products) {
                                Console.WriteLine("    " + product.id + ", " + product.stock);
                            }

                        }
                    }

                    Console.WriteLine("-----------------------------");
                    Console.WriteLine("Cancelled Orders");
                    Console.WriteLine("-----------------------------");

                    Console.WriteLine("Order ID, Customer ID, Order Date");
                    foreach (var order in orders) {
                        if(order.isCancelled) {
                            Console.WriteLine(order.id + ", " + order.customerId + ", " + order.orderDate);

                            Console.WriteLine("    Products:");
                            Console.WriteLine("    Product ID, Amount");

                            foreach (var product in order.products) {
                                Console.WriteLine("    " + product.id + ", " + product.stock);
                            }

                        }
                    }


                    break;
                }

                case 7: {
                    int orderId = GetUserInputNumber("Enter Order ID: ");

                    if (controller.CancelOrder(orderId)) {
                        Console.WriteLine("Order successfully cancelled!");
                    } else {
                        Console.WriteLine("Whoops! Error occurred.. Try again");
                    }


                    break;
                }

                case 8: {
                    int orderId = GetUserInputNumber("Enter Order ID: ");
                    
                    if(controller.DeleteOrder(orderId)) {
                        Console.WriteLine("Order successfully deleted!");
                    } else {
                        Console.WriteLine("Whoops! Error occurred.. Try again");
                    }

                    break;
                }

                case 9: {

                    if(order != null) {

                        Console.WriteLine("Products of Order: " + order.id);
                        Console.WriteLine("------------------------------------");
                        Console.WriteLine("Product ID, Name, Amount");
                        foreach (var product in order.products) {
                            Console.WriteLine(product.id + ", " + product.name + ", " + product.stock);
                        }

                    } else {
                        Console.WriteLine("Please add a product to the order!");
                    }

                    break;
                }
            }
        }

        public void Run() {
            try {
                int action = -1;

                do {
                    PrintMenu();

                    string input = Console.ReadLine();
                    action = Convert.ToInt32(input);

                    Handle(action);

                } while (action != 0);

            } catch (InvalidCastException) {

            }
        }

    }
}

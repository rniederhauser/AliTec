﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliTec.Controllers;
using AliTec.Database;
using AliTec.Models;

namespace AliTec.Views {
    class WarehouseView : IView {

        private WarehouseController controller;

        public WarehouseView() {
            controller = new WarehouseController();
        }

        public void PrintMenu() {
            Console.WriteLine();
            Console.WriteLine("Menu Warehouse Management");
            Console.WriteLine("----------------------------");
            Console.WriteLine("1 Enter a product");
            Console.WriteLine("2 Show a product");
            Console.WriteLine("3 Show all products");
            Console.WriteLine("4 Update a product");
            Console.WriteLine("5 Delete a product");
            Console.WriteLine("6 Show products with low stock");
            Console.WriteLine("7 Warehouse statistics");
            Console.WriteLine("0 Back");
        }

        // ask user for a valid id
        public int GetProductID() {
            bool success = false;
            int id = 0;
            while (!success) {
                try {
                    Console.Write("Product ID: ");
                    id = Convert.ToInt32(Console.ReadLine());
                    success = true;
                } catch (Exception e) {

                }
            }
            return id;
        }

        public void Run() {

            try {
                int action = -1;

                do {
                    PrintMenu();

                    string input = Console.ReadLine();
                    action = Convert.ToInt32(input);

                    Handle(action);

                } while (action != 0);

            } catch(Exception e) {

            }        
        }

        public void Handle(int action) {
            switch (action) {
                case 1: {
                    Console.WriteLine("Enter: Storage Site;Name;Category;Manufacturer;Stock;Price;Minimum Stock");
                    string[] data = Console.ReadLine().Split(';');

                    bool result = controller.CreateProduct(data);

                    if(result) {
                        Console.WriteLine("Product successfully added!");
                    } else {
                        Console.WriteLine("Whoops! Error occurred.. Try again!");
                    }

                    break;
                }

                case 2: {
                    int id = GetProductID();

                    Console.Write("With History? (y/n): ");
                    string response = Console.ReadLine();
                    bool withHistory = false;

                    if (response == "y") {
                        withHistory = true;
                    }

                    var products = controller.GetProduct(id, withHistory);

                    Console.WriteLine("--------------------------------");
                    Console.WriteLine("Real:");
                    Console.WriteLine("--------------------------------");
                    Console.WriteLine("Product ID, Storage Site, Name, Category, Manufacturer, Stock, Price, Minimum Stock");
                    foreach (var product in products.Item1) {
                        Console.WriteLine(product.id + ", " + product.storagesite + ", " + product.name + ", " + product.category + ", " + product.manufacturer + ", " + product.stock + ", " + product.price + ", " + product.minstock);
                    }

                    if(withHistory) {
                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("History:");
                        Console.WriteLine("--------------------------------");
                        foreach (var product in products.Item2) {
                            Console.WriteLine(product.id + ", " + product.storagesite + ", " + product.name + ", " + product.category + ", " + product.manufacturer + ", " + product.stock + ", " + product.price + ", " + product.minstock);
                        }
                    }

                    break;
                }

                case 3: {
                    Console.Write("With History? (y/n): ");
                    string response = Console.ReadLine();
                    bool withHistory = false;

                    if (response == "y") {
                        withHistory = true;
                    }

                    var products = controller.GetProducts(withHistory);

                    Console.WriteLine("--------------------------------");
                    Console.WriteLine("Real:");
                    Console.WriteLine("--------------------------------");
                    Console.WriteLine("Product ID, Storage Site, Name, Category, Manufacturer, Stock, Price, Minimum Stock");
                    foreach (var product in products.Item1) {
                        Console.WriteLine(product.id + ", " + product.storagesite + ", " + product.name + ", " + product.category + ", " + product.manufacturer + ", " + product.stock + ", " + product.price + ", " + product.minstock);
                    }

                    if(withHistory) {
                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("History:");
                        Console.WriteLine("--------------------------------");
                        Console.WriteLine("Product ID, Storage Site, Name, Category, Manufacturer, Stock, Price, Minimum Stock");
                        foreach (var product in products.Item2) {
                            Console.WriteLine(product.id + ", " + product.storagesite + ", " + product.name + ", " + product.category + ", " + product.manufacturer + ", " + product.stock + ", " + product.price + ", " + product.minstock);
                        }
                    }

                    break;
                }

                case 4: {
                    int id = GetProductID();

                    Console.WriteLine("Enter a '*' to keep the current value of the field!");
                    Console.WriteLine("Enter: Storage Site;Name;Category;Manufacturer;Stock;Price;Minimum Stock");
                    string[] data = Console.ReadLine().Split(';');

                    bool success = controller.UpdateProduct(id, data);

                    if(success) {
                        Console.WriteLine("Product successfully updated!");
                    } else {
                        Console.WriteLine("Whoops! Error occurred.. Try again!");
                    }

                    break;
                }

                case 5: {
                    int id = GetProductID();
                    bool success = controller.DeleteProduct(id);

                    if(success) {
                        Console.WriteLine("Product successfully deleted!");
                    } else {
                        Console.WriteLine("Whoops! Error occurred.. Try again!");
                    }

                    break;
                }

                case 6: {
                    var products = controller.GetProducts(false);

                    Console.WriteLine("Product ID, Storage Site, Name, Category, Manufacturer, Stock, Price, Minimum Stock");
                    foreach (var product in products.Item1) {
                        if(product.stock < product.minstock) {
                            Console.WriteLine(product.id + ", " + product.storagesite + ", " + product.name + ", " + product.category + ", " + product.manufacturer + ", " + product.stock + ", " + product.price + ", " + product.minstock);
                        }
                    }

                    break;
                }

                case 7: {
                    var products = controller.GetProducts(false);

                    double price = 0;
                    int stock = 0;

                    if(products.Item1.Count > 0) {

                        foreach (var product in products.Item1) {
                            price += product.price * product.stock;
                            stock += product.stock;
                        }

                        Console.WriteLine("Total amount of items: " + stock);
                        Console.WriteLine("Total worth of items: " + price.ToString("N2"));

                    } else {
                        Console.WriteLine("No product found!");
                    }


                    break;
                }
            }
        }
    }
}

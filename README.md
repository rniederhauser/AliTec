# How to use Database Functions

**Create a Connection**
Very easy..
`var conn = new DatabaseConnection();`

**Collection Class**
The Collection class contains two lists:
- resultList - Contains the result from the **real** file
- historyList - Contains the result from the **history** file

The properties can be easily accessed using:
`collection.resultList` or `collection.historyList`

**QueryBuilder Class**
The QueryBuilder requires one parameter for the constructor. It's the file that will be accessed during the DB Method (CRUD).
```csharp
QueryBuilder builder = new QueryBuilder(DatabaseCollections.WAREHOUSE)
																	.Build();
```
The files are in the DatabaseCollections Class. If you want to add a new file, feel free to append a new parameter by using copy paste.

The QueryBuilder class is documented with each attribute and function. Feel free to read it when needed.

**Cast a list of a collection**
We assume that we'd like to cast the result from the Select Function into a List of Product Objects:

`var productList = collection.resultList.Cast<Product>().ToList();`

We save the casted list into a new variable. The parameter of the .Cast<> is the Class that you would like to get the List Casted to. 

When the Cast is done, you can loop through the list and access the attributes of the Product class:
`Console.WriteLine(productList[0].name)`

**Select All From Database**

```csharp
var collection = conn.Select(new QueryBuilder(DatabaseCollections.WAREHOUSE).SelectAll().Build());

var products = collection.resultList.Cast<Product>().ToList();
```

**Select using Criterias From Database**

We use the QueryBuilder once again to query the data from the database:
```csharp
var collection = conn.Select(new QueryBuilder(DatabaseCollections.WAREHOUSE)
																			.Add("name", "OnePlus 5T")
																			.Build());
```
It's possible to use as many ``.Add("field", "value")`` as you need. There is no limit. The value field can be any data type.

**Select with History Result**

If you need to get the results of the history file, use .WithHistory():
```csharp
var collection = conn.Select(new QueryBuilder(DatabaseCollections.WAREHOUSE)
																			.Add("name", "OnePlus 5T")
							 												.WithHistory()
																			.Build());
```

**Insert into Database**

In order to insert Model Objects into the Database, use the following function of the DatabaseConnection instance:
```csharp
conn.Insert(new QueryBuilder(DatabaseCollections.WAREHOUSE)
                        .AddModel(new Product(123, "OnePlus 5T"))
                        .AddModel(new Product(44, "Samsung Galaxy S7"))
                        .Build());
```
You can even add a range (list) of models that will be inserted into the database:
```csharp
List<Model> insertList = new List<Model>();
insertList.Add(new Product(123, "OnePlus 5T"));
insertList.Add(new Product(44, "Samsung Galaxy S7"));

conn.Insert(new QueryBuilder(DatabaseCollections.WAREHOUSE)
												.AddModelRange(insertList)
												.Build());
```

**Update object of Database**

If you want to update an object of the database, use the following syntax:
```csharp
conn.Update(new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                        .Add("id", 123) // criteria, which model will be updated
                                        .AddUpdateField("name", "OnePlus 5") // updated field
                                        .Build());
```
Please consider that **the only criteria that can be used is the id** of the model.

**Delete object from Database**

If you want to delete an object of the database, use the following syntax:
```csharp
conn.Delete(new QueryBuilder(DatabaseCollections.WAREHOUSE)
                                        .Add("id", 123)
                                        .Build());
```
Please consider that **the only criteria that can be used is the id** of the model.